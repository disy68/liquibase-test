package hu.diskay;

import hu.diskay.configuration.ApplicationConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class Application {

    public static void main(String[] args) {

        new SpringApplicationBuilder(ApplicationConfiguration.class)
            .headless(false)
            .run(args)
            .start();

    }
}
