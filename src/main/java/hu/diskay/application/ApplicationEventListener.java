package hu.diskay.application;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;

public class ApplicationEventListener {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationEventListener.class);

    @EventListener
    public void startUpHandler(ContextStartedEvent contextStartedEvent) {
        System.out.println("http://localhost:10100/h2-console");
        try {
            URI uri = new URI("http://localhost:10100/h2-console");
            Desktop.getDesktop().browse(uri);
        } catch (IOException e) {
            LOG.info(e.getMessage());
        } catch (URISyntaxException e) {
            LOG.info(e.getMessage());
        }
    }

}
