package hu.diskay.configuration;

import hu.diskay.application.ApplicationEventListener;
import hu.diskay.controller.RandomController;
import java.util.Random;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApplicationConfiguration {

    @Bean
    public RandomController randomController() {
        return new RandomController(new Random());
    }

    @Bean
    public ApplicationEventListener applicationEventListener() {
        return new ApplicationEventListener();
    }
}
