package hu.diskay.controller;

import java.util.Random;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RandomController {

    private final Random random;

    public RandomController(Random random) {
        this.random = random;
    }

    @RequestMapping("/random")
    public String random() {
        return String.valueOf(random.nextFloat());
    }
}
